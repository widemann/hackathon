# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 08:54:52 2015

@author: widemann1
"""


from __future__ import print_function
import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.datasets.data_utils import get_file
import numpy as np
import random, sys


#%%
nsamples = 1
input_dim = 5
maxLen = 3
output_dim = 1

model = Sequential()
model.add(keras.layers.recurrent.LSTM(input_dim, output_dim=output_dim, 
        init='glorot_uniform', inner_init='orthogonal', forget_bias_init='one',
        activation='tanh', inner_activation='hard_sigmoid',
        weights=None, truncate_gradient=-1, return_sequences=False))
        
model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
        
data = np.random.randint(0,10,(nsamples,maxLen,input_dim))


y0 = model.predict(data,batch_size=1)
y0.shape


#%% return sequence

model_return_sequence = Sequential()
model_return_sequence.add(keras.layers.recurrent.LSTM(input_dim, output_dim=output_dim, 
        init='glorot_uniform', inner_init='orthogonal', forget_bias_init='one',
        activation='tanh', inner_activation='hard_sigmoid',
        weights=None, truncate_gradient=-1, return_sequences=True))
        
model_return_sequence.compile(loss='categorical_crossentropy', optimizer='rmsprop')
        

y1 = model_return_sequence.predict(data,batch_size=1)
y1.shape



